package com.image.imageupload.controller;

import com.image.imageupload.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;


@Controller
public class ImageController {
    @Autowired
    ImageService imageService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public void createImagePost() {
        File file = new File("/home/zoheb/java/imageupload/src/main/resources/background.jpeg");
        byte[] image = new byte[(int) file.length()];
        imageService.readImage(image, file);
    }

}
