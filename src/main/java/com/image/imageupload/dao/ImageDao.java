package com.image.imageupload.dao;

import com.image.imageupload.domain.Image;
import org.springframework.data.repository.CrudRepository;

public interface ImageDao extends CrudRepository<Image,Integer> {
}
