package com.image.imageupload.service;

import org.springframework.stereotype.Service;

import java.io.File;


public interface ImageService {
    void readImage(byte[] image,
                   File path);
}
